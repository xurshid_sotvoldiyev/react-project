import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import FunctionalComponentProvider from "./Components/Functional component/ContextFunctionalComponet";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <BrowserRouter>
    <FunctionalComponentProvider>
      <App />
    </FunctionalComponentProvider>
  </BrowserRouter>
);
