import React from "react";
import { Link, useParams } from "react-router-dom";

export default function Header() {
  return (
    <div className="buttons">
      <Link to={"/functional"}>
        <button className="header_btn">Functional Component</button>
      </Link>
      <Link to={"/class"}>
        <button className="header_btn">Class Component</button>
      </Link>
    </div>
  );
}
