import React, { Component } from 'react'

export default class TableHead extends Component {
  render() {
    return (
        <thead>
        <tr>
          <th>
            <h1>№</h1>
          </th>
          <th>
            <h1>Name</h1>
          </th>
          <th>
            <h1>Email</h1>
          </th>
          <th>
            <h1>Body</h1>
          </th>
        </tr>
      </thead>
    )
  }
}
