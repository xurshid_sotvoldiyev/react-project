import React, { Component } from "react";
import Repositories from "../../../Repositories/Repositories";
import Pagination from "./Pagination";
import TableBody from "./TableBody";
import TableHead from "./TableHead";

export default class ClassComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      items: null,
      pages: 1,
      currentPage: 1,
    };
  }
  async componentDidMount() {
    console.log(this.state.bodySearch);
    const getAllData = await Repositories.getAllData();
    const pageQuantities = getAllData?.data?.length / 10;
    const items = getAllData.data?.slice(0, 10);
    this.setState({
      data: getAllData.data,
      items: items,
      pages: pageQuantities,
    });
  }
  onChangeBody = (e) => {
    const result = this.state.data?.filter((element) => {
      return element.body.toLowerCase().match(e.target.value?.toLowerCase());
    });
    console.log(result);
    this.setState({ items: result });
  };
  onChangeName = (e) => {
    const result = this.state.data?.filter((element) => {
      return element.name.toLowerCase().match(e.target.value?.toLowerCase());
    });
    console.log(result);
    this.setState({ items: result });
  };

  paginateTo = (page) => {
    this.setState({ currentPage: page });
    const index = (page - 1) * 10;
    const newPageData = this.state.data?.slice(index, index + 10);
    this.setState({ items: newPageData });
  };

  render() {
    return (
      <>
        <div className="search_bar_conatiner">
          <input
            type="text"
            placeholder="Search Name"
            className="search_bar"
            value={this.state.nameSearch}
            onChange={(e) => this.onChangeName(e)}
          />
          <input
            type="text"
            placeholder="Search Body"
            className="search_bar"
            value={this.state.bodySearch}
            onChange={(e) => this.onChangeBody(e)}
          />
        </div>
        <table className="containerTable">
          <TableHead />
          {this.state.items?.map((element) => {
            return <TableBody key={element.id} element={element} />;
          })}
        </table>
        <Pagination state={this.state} callback={this.paginateTo} />
      </>
    );
  }
}
