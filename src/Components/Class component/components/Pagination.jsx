import React, { Component } from "react";

export default class Pagination extends Component {
  render() {
    const paginationItems = [];

    for (let i = 1; i <= this.props.state?.pages; i++) {
      paginationItems.push(
        <li key={i}>
          <div
            className={
              this.props.state?.currentPage === i
                ? "pagination__item c-pointer pagination__item_active"
                : "pagination__item c-pointer"
            }
            onClick={
              this.props.state?.currentPage === i
                ? null
                : () => this.props.callback(i)
            }
          >
            {i}
          </div>
        </li>
      );
    }
    const items = paginationItems?.slice(
      this.props.state?.currentPage - 1,
      this.props.state?.currentPage + 9
    );
    if (this.props.state?.pages !== 1)
      return (
        <ul
          className={
            this.props.state?.items?.length < 10 ||
            this.props.state?.items?.length > 10
              ? "d-none"
              : "pagination"
          }
        >
          {this.props.state?.pages > 1 ? (
            <li>
              <div
                className="pagination__item c-pointer"
                onClick={
                  this.props.state?.currentPage === 1
                    ? null
                    : () =>
                        this.props.callback(this.props.state?.currentPage - 1)
                }
              >
                <img
                  src="/image/pagination-arrow-left.svg"
                  alt="pagination icon"
                />
              </div>
            </li>
          ) : null}

          {items}
          {this.props.state?.currentPage <= this.props.state?.pages ? (
            <li>
              <div
                className="pagination__item c-pointer"
                onClick={
                  this.props.state?.currentPage === 50
                    ? () => this.props.callback(1)
                    : () =>
                        this.props.callback(this.props.state?.currentPage + 1)
                }
              >
                <img
                  src="/image/pagination-arrow-right.svg"
                  alt="pagination icon"
                />
              </div>
            </li>
          ) : null}
        </ul>
      );
    return null;
  }
}
