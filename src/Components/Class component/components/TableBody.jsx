import React, { Component } from "react";

export default class TableBody extends Component {
  render() {
    return (
      <tbody>
        <tr>
          <td>{this.props.element.id}</td>
          <td>{this.props.element.name}</td>
          <td>{this.props.element.email}</td>
          <td>{this.props.element.body}</td>
        </tr>
      </tbody>
    );
  }
}
