import FunctionalComponentAction from "./ActionsFunctionalComponet";

export const functionalComponentReducers = (state, action) => {
  switch (action.type) {
    case FunctionalComponentAction.PAGE_LOAD:
      return { ...state, ...action.payload };
    case FunctionalComponentAction.ADD_QUERY_PARAM:
      return {
        ...state,
        currentPage: action.payload,
      };
    case FunctionalComponentAction.CHANGE_FILTERED_DATA:
      return {
        ...state,
        items: action.payload,
      };
    default:
      return state;
  }
};
