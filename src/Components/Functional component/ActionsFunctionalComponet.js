export default class FunctionalComponentAction{
    static PAGE_LOAD="PAGE_LOAD"
    static ADD_QUERY_PARAM="ADD_QUERY_PARAM"
    static CHANGE_FILTERED_DATA="CHANGE_FILTERED_DATA"
}