import React from "react";

export default function Pagination({ context, callback }) {
  const paginationItems = [];

  for (let i = 1; i <= context.pages; i++) {
    paginationItems.push(
      <li key={i}>
        <div
          className={
            context.currentPage === i
              ? "pagination__item c-pointer pagination__item_active"
              : "pagination__item c-pointer"
          }
          onClick={context.currentPage === i ? null : () => callback(i)}
        >
          {i}
        </div>
      </li>
    );
  }
  const items = paginationItems?.slice(
    context.currentPage - 1,
    context.currentPage + 9
  );
  if (context.pages !== 1)
    return (
      <ul
        className={
          context.items?.length < 10 || context.items?.length > 10
            ? "d-none"
            : "pagination"
        }
      >
        {context.pages > 1 ? (
          <li>
            <div
              className="pagination__item c-pointer"
              onClick={
                context.currentPage === 1
                  ? null
                  : () => callback(context.currentPage - 1)
              }
            >
              <img
                src="/image/pagination-arrow-left.svg"
                alt="pagination icon"
              />
            </div>
          </li>
        ) : null}

        {items}
        {context.currentPage <= context.pages ? (
          <li>
            <div
              className="pagination__item c-pointer"
              onClick={
                context.currentPage === 50
                  ? () => callback(1)
                  : () => callback(context.currentPage + 1)
              }
            >
              <img
                src="/image/pagination-arrow-right.svg"
                alt="pagination icon"
              />
            </div>
          </li>
        ) : null}
      </ul>
    );
  return null;
}
