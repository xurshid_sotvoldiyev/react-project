import React, { useCallback, useContext, useEffect, useState } from "react";
import { FunctionalComponentContext } from "../ContextFunctionalComponet";
import { loadItems, onReload } from "../actions";
import TableBody from "./TableBody";
import TableHead from "./TableHead";
import FunctionalComponentAction from "../ActionsFunctionalComponet";
import Pagination from "./Pagination";

export default function FunctionalComponent() {
  const { functionalComponent, dispatchFunctionalComponent } = useContext(
    FunctionalComponentContext
  );
  const [nameSearch, setNameSearch] = useState("");
  const [bodySearch, setBodySearch] = useState("");
  console.log(nameSearch);
  console.log(bodySearch);
  const pageReload = async () => {
    await onReload(dispatchFunctionalComponent);
  };

  useEffect(() => {
    pageReload();
  }, []);

  useEffect(() => {
    const result = functionalComponent.data?.filter((element) => {
      return element.name.toLowerCase().match(nameSearch?.toLowerCase());
    });
    dispatchFunctionalComponent({
      type: FunctionalComponentAction.CHANGE_FILTERED_DATA,
      payload: result,
    });
  }, [nameSearch]);

  useEffect(() => {
    const result = functionalComponent.data?.filter((element) => {
      return element.body.toLowerCase().match(bodySearch?.toLowerCase());
    });
    dispatchFunctionalComponent({
      type: FunctionalComponentAction.CHANGE_FILTERED_DATA,
      payload: result,
    });
  }, [bodySearch]);

  const paginateTo = useCallback((page) => {
    dispatchFunctionalComponent({
      type: FunctionalComponentAction.ADD_QUERY_PARAM,
      payload: page,
    });
    loadItems(page, functionalComponent, dispatchFunctionalComponent);
  });

  return (
    <>
      <div className="search_bar_conatiner">
        <input
          type="text"
          placeholder="Search Name"
          className="search_bar"
          value={nameSearch}
          onChange={(e) => setNameSearch(e.target.value)}
        />
        <input
          type="text"
          placeholder="Search Body"
          className="search_bar"
          value={bodySearch}
          onChange={(e) => setBodySearch(e.target.value)}
        />
      </div>
      <table className="containerTable">
        <TableHead />
        {functionalComponent.items?.map((element) => {
          return <TableBody key={element.id} element={element} />;
        })}
      </table>
      <Pagination context={functionalComponent} callback={paginateTo} />
    </>
  );
}
