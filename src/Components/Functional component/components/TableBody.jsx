import React from "react";

export default function TableBody({element}) {
  return (
    <tbody>
      <tr>
        <td>{element.id}</td>
        <td>{element.name}</td>
        <td>{element.email}</td>
        <td>{element.body}</td>
      </tr>
    </tbody>
  );
}
