import { createContext, useReducer } from "react";
import { functionalComponentReducers } from "./ReducerFunctionalContext";
export const FunctionalComponentContext = createContext();

const initialState = {
  data: null,
  items: null,
  pages: 1, 
  currentPage: 1,
};
export default function FunctionalComponentProvider(props) {
  const [functionalComponent, dispatchFunctionalComponent] = useReducer(
    functionalComponentReducers,
    {
      ...initialState,
    }
  );
  return (
    <FunctionalComponentContext.Provider
      value={{ functionalComponent, dispatchFunctionalComponent }}
    >
      {props.children}
    </FunctionalComponentContext.Provider>
  );
}
