import FunctionalComponentAction from "./ActionsFunctionalComponet";
import Repositories from "../../Repositories/Repositories";

export const onReload = async (dispatch) => {
  const getAllData = await Repositories.getAllData();
  const pageQuantities = getAllData?.data?.length / 10;
  console.log(pageQuantities);
  const items = getAllData.data?.slice(0, 10);
  dispatch({
    type: FunctionalComponentAction.PAGE_LOAD,
    payload: {
      data: getAllData.data,
      items: items,
      pages: pageQuantities,
    },
  });
};
export const loadItems = async (payload, context, dispatch) => {
  const index = (payload - 1) * 10 ;
  const newPageData = context.data?.slice(index, index + 10);
  dispatch({
    type: FunctionalComponentAction.PAGE_LOAD,
    payload: {
      items: newPageData,
    },
  });
};
