import axiosInstance from "./AxiosInstance";

export default class Repositories {
  static async getAllData() {
    let result = {
      status: false,
      data: null,
    };
    const response = await axiosInstance.get();
    if (response.status === 200) {
      result = { ...result, status: true, data: response.data };
    }
    return result;
  }
}
