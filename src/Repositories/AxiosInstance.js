import axios from "axios";
import { BASE_API_URL } from "./Constants";

const axiosInstance = axios.create({
  baseURL: BASE_API_URL,
  headers: {
    "Accept": "application/json",
    "Content-type": "application/json",
  }
});

export default axiosInstance;
