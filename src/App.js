import { Route, Routes, Navigate } from "react-router-dom";
import ClassComponent from "./Components/Class component/components/ClassComponent";
import FunctionalComponent from "./Components/Functional component/components/FunctionalComponet";
import Header from "./Header";
function App() {
  return (
    <div className="container">
      <Header />
      <Routes>
        <Route
          path="/"
          render={() => {
            return <Navigate to="/functional" />;
          }}
          element={<FunctionalComponent />}
        />
        <Route path="/functional" element={<FunctionalComponent />} />
        <Route path="/class" element={<ClassComponent />} />
      </Routes>
    </div>
  );
}

export default App;
